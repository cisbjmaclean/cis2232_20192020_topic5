package cis;

import cis.bo.TireJob;
import cis.util.CisUtility;

/**
 * A starting project which we can use for applications that need a menu driven
 * program. Note that the name of the project should be modified to reflect the
 * specific requirements.
 *
 * @author bjmaclean
 * @since 20181115
 * 
 * @modified BJM 20191124 - Conversion for CIS Automotive activity.
 */
public class Controller {
    
    public static final String EXIT = "X";
    
    private static final String MENU
            = "-------------------------\n"
            + "- CIS Automotive\n"
            + "- A-Add a new job\n"
            + "- B-Show last job\n"
            + "- X-eXit\n"
            + "-------------------------\n"
            + "Option";
    
    private static TireJob job = new TireJob();
    
    public static void main(String[] args) {

        //Add a loop below to continuously promput the user for their choice 
        //until they choose to exit.
        String option = "";

        //CisUtility.setIsGUI(true);
        
        do {
            option = CisUtility.getInputString(MENU);
            processMenuOption(option);
        } while (!option.equalsIgnoreCase(EXIT));
        
    }

    /**
     * This method will process the menu option specified in the input
     * parameter. It will call appropriate functionality based on its value.
     *
     * @param option The menu option
     * @since 20171102
     * @author cis1201b
     *
     */
    public static void processMenuOption(String option) {
        //Add a switch to process the option
        switch (option.toUpperCase()) {
            case "A":
                job = addJob();
                break;
            case "B":
                job.display();
                break;
            case "X":
                CisUtility.display("Thanks for using the system.");
                break;
            default:
                CisUtility.display("Invalid entry");
        }
    }
    
    /**
     * Add a new job.
     * @since 20191124
     * @author BJM
     */
    public static TireJob addJob(){
        TireJob job = new TireJob();
        job.getData();
        job.display();
        return job;
    }
}
