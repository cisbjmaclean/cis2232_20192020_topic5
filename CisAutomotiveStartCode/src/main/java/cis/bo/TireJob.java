package cis.bo;

import cis.util.CisUtility;

/**
 * Tire to process the tire change jobs for Cis Automotive
 *
 * @author bjm
 * @since 20191124
 */
public class TireJob {

    //Attributes
    private double baseCost, netCost;
    private int yearOfJob, monthOfJob, dayOfJob;
    private int numberOfTiresBeingChanged;
    private int typeOfVehicle;  //1=car 2=truck 3=van
    private int customerNumber;
    private boolean withSensors;
    private String customerName;

    private double premiumRatePrimeTime = 0.0;
    private double premiumRateForVehicleType = 0.0;
    private double discountRateTireQuantity = 0.0;
    private double discountRateCustomerType = 0.0;
    private double premiumRateHasSensors = 0.0;

    //Constants
    public static final double BASE_COST_PER_TIRE = 20;

    public static final double DISCOUNT_3_OR_MORE = 0.05;
    public static final double DISCOUNT_GOLD = 0.1;
    public static final double DISCOUNT_PLATINUM = 0.2;

    public static final double PREMIUM_TRUCK = 0.2;
    public static final double PREMIUM_VAN = 0.3;
    public static final double PREMIUM_PRIME_TIME = 0.25;
    public static final double PREMIUM_SENSORS = 0.1;
    
    public static final int TYPE_CAR = 1;
    public static final int TYPE_TRUCK = 2;
    public static final int TYPE_VAN = 3;


    /**
     * sets discount rate applicable for number of tires. if three or
     * four tires are being changed there is a 5% discount
     *
     * @since 20191124
     * @author BJM
     */
    public void setDiscountRateTireQuantity() {

        //**********************************************************************
        //TODO
        //Apply logic to return the discount rate based on number of tires.
        //The rate would be 0 or 0.05 based on the tire quantity.
        //**********************************************************************
        
        
        
        
        discountRateTireQuantity= 0.005; //This is wrong
    }

    /**
     * sets the premium rate for vehicle type. There is a 20% premium on the
     * base cost for installing tires on trucks and a 30% premium on the base
     * cost for vans.
     *
     * @since 20191124
     * @author BJM
     */
    public void setPremiumRateVehicleType() {

        //**********************************************************************
        //TODO
        //Apply logic to set the premium rate for vehicle type.  
        //Note that the rate will be 0, 0.2, or 0.3.
        //**********************************************************************
        
        //USE A SWITCH STATEMENT
        
        
        premiumRateForVehicleType= 0.005; //This is wrong

    }

    /**
     * sets the discount rate for customer number. Gold customers #1000-1999
     * get 10% discount off the base cost. Platinum customers #2000-2999 get 20%
     * off the base cost.
     *
     * @since 20191124
     * @author BJM
     */
    public void setDiscountRateCustomerType() {

        //**********************************************************************
        //TODO
        //Apply logic to set the discount rate.  
        //Note the rate will be 0, 0.1, or 0.2 based on customer number.
        //**********************************************************************
        
        
        
        discountRateCustomerType= 0.005; //This is wrong

    }

    /**
     * Determine if special order. If the order is for a truck or van and four
     * tires then the order is considered a special job.
     *
     * @since 20191124
     * @author BJM
     */
    public boolean determineIfIsSpecialJob() {

        //**********************************************************************
        //TODO
        //Apply logic to return true or false appropriately
        //**********************************************************************


        return false;
    }

    /**
     * A 25% premium is applied between November 1st and 15th (inclusive).
     *
     * @return The applicable premium rate (0 or 25%)
     * @since 20191124
     * @author BJM
     */
    public void setPremiumRatePrimeTime() {

        //**********************************************************************
        //TODO
        //Apply logic to set the premium rate (ie. 0 or 25%)
        //**********************************************************************

        premiumRatePrimeTime = 0.005; //This is wrong

    }

    /**
     * Determine and set the tire sensor premium. If the tires have sensors
     * than there is a 10% premium to be applied on the job.
     *
     * @since 20191124
     * @author BJM
     */
    public void setPremiumRateTireSensor() {

        //**********************************************************************
        //TODO 
        //Apply logic to set the premium rate if the tires have sensors.
        //Note the rate will be 0 or 0.1
        //**********************************************************************

        premiumRateHasSensors = 0.005; //This is wrong
    }

    
    
    
    //**************************************************************************
    //
    //
    // Nothing below should have to be changed.
    //
    //
    //**************************************************************************
    
    
    
    /**
     * Set and return the base cost of this job.
     *
     * @return the base cost after setting the attribute
     * @since 20191124
     * @author BJM
     */
    public void setBaseCost() {
        baseCost = numberOfTiresBeingChanged * BASE_COST_PER_TIRE;
    }

    /**
     * Get attributes values from the user
     *
     * @since 520191124
     * @author BJM
     */
    public void getData() {
        CisUtility.display("Please provide job details.");

        customerName = CisUtility.getInputString("Enter customer name");

        yearOfJob = CisUtility.getInputInt("Enter year");
        monthOfJob = CisUtility.getInputInt("Enter month");
        dayOfJob = CisUtility.getInputInt("Enter day");

        numberOfTiresBeingChanged = CisUtility.getInputInt("How many tires are being changed?");
        typeOfVehicle = CisUtility.getInputInt("Type of vehicle (1=car 2-truck 3=van)?");
        customerNumber = CisUtility.getInputInt("Enter customer number");
        withSensors = CisUtility.getInputString("Do the tires have sensors (y/n)").equalsIgnoreCase("y");
        
        setBaseCost();
        setDiscountRateTireQuantity();
        setPremiumRateVehicleType(); 
        setDiscountRateCustomerType();
        setPremiumRatePrimeTime();
        setPremiumRateTireSensor();
        
        netCost = baseCost 
                - baseCost*discountRateCustomerType
                - baseCost*discountRateTireQuantity
                + baseCost*premiumRateForVehicleType
                + baseCost*premiumRateHasSensors
                + baseCost*premiumRatePrimeTime;
    }

    //Getters/Setters

    public double getBaseCost() {
        return baseCost;
    }

    public double getNetCost() {
        return netCost;
    }

    public int getYearOfJob() {
        return yearOfJob;
    }

    public int getMonthOfJob() {
        return monthOfJob;
    }

    public int getDayOfJob() {
        return dayOfJob;
    }

    public int getNumberOfTiresBeingChanged() {
        return numberOfTiresBeingChanged;
    }

    public int getCustomerNumber() {
        return customerNumber;
    }

    public boolean isWithSensors() {
        return withSensors;
    }

    public String getCustomerName() {
        return customerName;
    }

    public double getPremiumRatePrimeTime() {
        return premiumRatePrimeTime;
    }

    public double getPremiumRateForVehicleType() {
        return premiumRateForVehicleType;
    }

    public double getDiscountRateTireQuantity() {
        return discountRateTireQuantity;
    }

    public double getDiscountRateCustomerType() {
        return discountRateCustomerType;
    }

    public double getPremiumRateHasSensors() {
        return premiumRateHasSensors;
    }

    public void setBaseCost(double baseCost) {
        this.baseCost = baseCost;
    }

    public void setYearOfJob(int yearOfJob) {
        this.yearOfJob = yearOfJob;
    }

    public void setMonthOfJob(int monthOfJob) {
        this.monthOfJob = monthOfJob;
    }

    public void setDayOfJob(int dayOfJob) {
        this.dayOfJob = dayOfJob;
    }

    public void setNumberOfTiresBeingChanged(int numberOfTiresBeingChanged) {
        this.numberOfTiresBeingChanged = numberOfTiresBeingChanged;
    }

    public void setTypeOfVehicle(int typeOfVehicle) {
        this.typeOfVehicle = typeOfVehicle;
    }

    public void setCustomerNumber(int customerNumber) {
        this.customerNumber = customerNumber;
    }

    public void setWithSensors(boolean withSensors) {
        this.withSensors = withSensors;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
    
    
    
    
    
    public void display() {
        CisUtility.display(toString());
    }

    public String toString() {

        String output = "\n--- Job Details ---\n"
                + "Name: " + customerName + " (#" + customerNumber + ")\n"
                + "Job Date: "+yearOfJob+"-"+monthOfJob+"-"+dayOfJob+"\n"
                + "--> Base cost: $%.2f\n"
                + "Tire quantity discount: (%.0f percent) $%.2f\n"
                + "Vehicle Type Premium: (%.0f percent) $%.2f\n"
                + "Customer type (Gold/Premium) discount: (%.0f percent) $%.2f\n"
                + "Is special job: " + determineIfIsSpecialJob() + "\n"
                + "Prime time premium: (%.0f percent) $%.2f\n"
                + "Tire Sensor premium: (%.0f percent) $%.2f\n"
                + "--> Net cost=$%.2f"
                + "\n\n";

        output = String.format(output, 
          baseCost,
          discountRateTireQuantity*100, baseCost*discountRateTireQuantity,
          premiumRateForVehicleType*100, baseCost*premiumRateForVehicleType,
          discountRateCustomerType*100, baseCost*discountRateCustomerType,
          premiumRatePrimeTime*100, baseCost*premiumRatePrimeTime,
          premiumRateHasSensors*100, baseCost*premiumRateHasSensors,
          netCost);

        return output;
    }
}
